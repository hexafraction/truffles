package com.wordpress.hextruffle.minilang;

public class ReturnValue extends Throwable {
	private Object rVal;

	public ReturnValue(Object rVal) {
		super();
		this.rVal = rVal;
	}

	public Object getValue() {
		return rVal;
	}
	
}
