package com.wordpress.hextruffle.minilang;

import javax.management.RuntimeErrorException;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.RootNode;

public class RootWrapperNode extends RootNode {

	private @Child VoidNode nodeToCall;
	@Override
	public Object execute(VirtualFrame frame) {
		try {
			nodeToCall.execVoid(frame);
		} catch (ReturnValue rv){
			return rv.getValue();
		} catch (Break b){
			throw new RuntimeException("Caught break outside a loop!");
		}
		return null;
	}
	public RootWrapperNode(VoidNode nodeToCall) {
		super();
		this.nodeToCall = nodeToCall;
	}

}
