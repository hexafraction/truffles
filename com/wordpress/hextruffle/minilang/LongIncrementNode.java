package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.frame.FrameSlotTypeException;
import com.oracle.truffle.api.frame.VirtualFrame;

public class LongIncrementNode extends LongValueNode {
	private final Object key;

	public LongIncrementNode(Object key) {
		this.key = key;
	}

	@Override
	public long execLong(VirtualFrame frame) throws ReturnValue, Break {
		ResolvedLongIncrementNode res = new ResolvedLongIncrementNode(key, frame.getFrameDescriptor().findFrameSlot(key));
		this.replace(res);
		return res.execLong(frame);
	}
	public static class ResolvedLongIncrementNode extends LongValueNode {
		private final Object key;
		private final FrameSlot slot;
		public ResolvedLongIncrementNode(Object key, FrameSlot slot) {
			super();
			this.key = key;
			this.slot = slot;
		}
		@Override
		public long execLong(VirtualFrame frame) throws ReturnValue, Break {
			long old;
			try {
				old = frame.getLong(slot);
			} catch (FrameSlotTypeException e) {

				System.out.println("DANGER! FSTE in LongIncrementNode:" + key);
				ResolvedLongIncrementNode res = new ResolvedLongIncrementNode(key, frame.getFrameDescriptor().findFrameSlot(key));
				this.replace(res);
				return res.execLong(frame);
			}
			long newVal = old + 1;
			FrameSlot slot = frame.getFrameDescriptor().findFrameSlot(key);
			frame.setLong(slot, newVal);
			return newVal;

		}
	}

}
