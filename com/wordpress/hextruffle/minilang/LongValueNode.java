package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;

public abstract class LongValueNode extends VoidNode {
	public abstract long execLong(VirtualFrame frame) throws ReturnValue, Break;

	public void execVoid(VirtualFrame frame) throws ReturnValue, Break {
		execLong(frame);
	}

}
