package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.frame.FrameSlotKind;
import com.oracle.truffle.api.frame.VirtualFrame;

public class LongVarDeclNode extends VoidNode {
	private @Child LongValueNode initialVal;
	private final Object key;
	private FrameSlot slot;
	public LongVarDeclNode(Object key, LongValueNode initialVal) {
		super();
		this.key = key;
		this.initialVal = initialVal;
		
		
	}
	@Override
	public void execVoid(VirtualFrame frame) throws ReturnValue, Break {
		FrameDescriptor desc = frame.getFrameDescriptor();
		this.slot = desc.addFrameSlot(key, FrameSlotKind.Long);
		frame.setLong(slot, initialVal.execLong(frame));
		ResolvedLongVarDeclNode newNode = new ResolvedLongVarDeclNode(key, initialVal, desc, slot);
		newNode.execVoid(frame);
	}
	
	public static class ResolvedLongVarDeclNode extends VoidNode {
		private @Child LongValueNode initialVal;
		private final Object key;
		private final FrameDescriptor desc;
		private final FrameSlot slot;
		public ResolvedLongVarDeclNode(Object key, LongValueNode initialVal, FrameDescriptor desc, FrameSlot slot) {
			super();
			this.key = key;
			this.initialVal = initialVal;
			this.desc = desc;
			this.slot = slot;
		}
		@Override
		public void execVoid(VirtualFrame frame) throws ReturnValue, Break {
			if(!frame.isLong(slot)){
				FrameDescriptor desc = frame.getFrameDescriptor();
				FrameSlot slot = desc.addFrameSlot(key, FrameSlotKind.Long);
				frame.setLong(slot, initialVal.execLong(frame));
				ResolvedLongVarDeclNode newNode = new ResolvedLongVarDeclNode(key, initialVal, desc, slot);
				newNode.execVoid(frame);
			}
			frame.setLong(slot, initialVal.execLong(frame));
		}
	}

}
