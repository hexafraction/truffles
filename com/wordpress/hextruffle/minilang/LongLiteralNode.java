package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.VirtualFrame;

public class LongLiteralNode extends LongValueNode {
	final long val;
	
	@Override
	public long execLong(VirtualFrame frame) throws ReturnValue, Break {
		return val;
	}

	public LongLiteralNode(long val) {
		super();
		this.val = val;
	}

}
