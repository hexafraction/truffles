package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;

public abstract class ObjectValueNode extends VoidNode {
	public abstract Object exec(VirtualFrame frame) throws ReturnValue, Break;
	@Override
	public void execVoid(VirtualFrame frame) throws ReturnValue, Break{
		exec(frame);
	}
}
