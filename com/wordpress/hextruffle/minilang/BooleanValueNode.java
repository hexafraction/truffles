package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.VirtualFrame;

public abstract class BooleanValueNode extends VoidNode {
	public abstract boolean execBool(VirtualFrame frame) throws ReturnValue, Break;

	public void execVoid(VirtualFrame frame) throws ReturnValue, Break {
		execBool(frame);
	}

}
