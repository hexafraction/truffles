package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.wordpress.hextruffle.minilang.LongValueNode;

public class ForLoopNode extends VoidNode {
	private @Child VoidNode initializer;
	private @Child BooleanValueNode loopCondition;
	private @Child VoidNode iterationAction;
	private @Child VoidNode loopBody;
	public ForLoopNode(VoidNode initializer, BooleanValueNode loopCondition, VoidNode iterationAction, VoidNode loopBody) {
		super();
		this.initializer = initializer;
		this.loopCondition = loopCondition;
		this.iterationAction = iterationAction;
		this.loopBody = loopBody;
	}
	@Override
	public void execVoid(VirtualFrame frame) throws ReturnValue, Break {
		for(this.initializer.execVoid(frame); this.loopCondition.execBool(frame); this.iterationAction.execVoid(frame)){
			this.loopBody.execVoid(frame);
		}
		
	}

}
