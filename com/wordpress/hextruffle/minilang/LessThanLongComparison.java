package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.VirtualFrame;

public class LessThanLongComparison extends BooleanValueNode {
	private @Child LongValueNode left;
	private @Child LongValueNode right;
	public LessThanLongComparison(LongValueNode left, LongValueNode right) {
		super();
		this.left = left;
		this.right = right;
	}
	@Override
	public boolean execBool(VirtualFrame frame) throws ReturnValue, Break {
		return left.execLong(frame) < right.execLong(frame);
	}

}
