package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.frame.FrameSlotTypeException;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.LineLocation;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.source.SourceSection;

public class LongVarReadNode extends LongValueNode{

	private final Object key;

	public LongVarReadNode(Object key) {
		super();
		this.key = key;
	}
	@Override
	public long execLong(VirtualFrame frame) throws ReturnValue, Break {
		ResolvedLongVarReadNode r = new ResolvedLongVarReadNode(key, frame.getFrameDescriptor().findFrameSlot(key));
		this.replace(r);
		return r.execLong(frame);
	}
	
	public static class ResolvedLongVarReadNode extends LongValueNode {
		private final Object key;
		private final FrameSlot slot;
		public long execLong(VirtualFrame frame){
			try {
				return frame.getLong(slot);
			} catch (FrameSlotTypeException e) {
				System.out.println("DANGER! FSTE in LongVarReadNode:"+key);
				ResolvedLongVarReadNode r = new ResolvedLongVarReadNode(key, frame.getFrameDescriptor().findFrameSlot(key));
				this.replace(r);
				return r.execLong(frame);
			}
		}
		public ResolvedLongVarReadNode(Object key, FrameSlot slot) {
			super();
			this.key = key;
			this.slot = slot;
		}
	}

}
