package com.wordpress.hextruffle.minilang;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;

public abstract class VoidNode extends Node {
	public abstract void execVoid(VirtualFrame frame) throws ReturnValue, Break;
}
