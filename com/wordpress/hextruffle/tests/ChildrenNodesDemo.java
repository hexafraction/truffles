package com.wordpress.hextruffle.tests;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.TruffleRuntime;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.RootNode;

public class ChildrenNodesDemo {

	static class StringConcatenatorTestRootNode extends RootNode {

		@Children
		private final TestChildNode[] children;

		public StringConcatenatorTestRootNode(TestChildNode[] children) {
			super(null);
			this.children = children;
		}

		@Override
		public Object execute(VirtualFrame frame) {
			String sum = "";
			for (TestChildNode child : children) {
				sum += child.execute();
			}
			return sum;
		}
	}

	static class TestChildNode extends Node {
		private final String s;

		public TestChildNode(String s) {
			super(null);
			this.s = s;
		}

		public String execute() {
			return s;
		}
	}

	public static void main(String[] args) {
		TruffleRuntime runtime = Truffle.getRuntime();
	        TestChildNode firstChild = new TestChildNode("Hello");
	        TestChildNode secondChild = new TestChildNode(", ");
	        TestChildNode thirdChild = new TestChildNode("world");
	        TestChildNode forthChild = new TestChildNode("!");
	        StringConcatenatorTestRootNode root = new StringConcatenatorTestRootNode(new TestChildNode[]{firstChild,secondChild,thirdChild,forthChild});
	        CallTarget target = runtime.createCallTarget(root);
	        System.out.println(target.call());
	}
}
