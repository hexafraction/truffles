package com.wordpress.hextruffle.tests;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.wordpress.hextruffle.minilang.Break;
import com.wordpress.hextruffle.minilang.ReturnValue;
import com.wordpress.hextruffle.minilang.VoidNode;

public class CompoundOperationNode extends VoidNode {
	private final @Children VoidNode[] children;
	
	public CompoundOperationNode(VoidNode... children) {
		this.children = children;
	}
	
	@Override
	public void execVoid(VirtualFrame frame) throws ReturnValue, Break {
		for(VoidNode child:children){
			child.execVoid(frame);
		}
	}

}
