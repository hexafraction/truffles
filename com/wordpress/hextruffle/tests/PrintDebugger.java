package com.wordpress.hextruffle.tests;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.wordpress.hextruffle.minilang.Break;
import com.wordpress.hextruffle.minilang.LongValueNode;
import com.wordpress.hextruffle.minilang.ReturnValue;
import com.wordpress.hextruffle.minilang.VoidNode;

public class PrintDebugger extends VoidNode {
	private static final PrintWriter w = new PrintWriter(new OutputStream() {
		
		@Override
		public void write(int b) throws IOException {
			// noop
		}
	});
	private @Child LongValueNode val;
	
	public PrintDebugger(LongValueNode val) {
		super();
		this.val = val;
	}

	@Override
	public void execVoid(VirtualFrame frame) throws ReturnValue, Break {
		w.println(this.val.execLong(frame));
	}

}
