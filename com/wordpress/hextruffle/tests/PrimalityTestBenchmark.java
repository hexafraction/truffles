package com.wordpress.hextruffle.tests;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.TruffleRuntime;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.nodes.DirectCallNode;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.RootNode;
import com.wordpress.hextruffle.minilang.ForLoopNode;
import com.wordpress.hextruffle.minilang.LessThanLongComparison;
import com.wordpress.hextruffle.minilang.LongIncrementNode;
import com.wordpress.hextruffle.minilang.LongLiteralNode;
import com.wordpress.hextruffle.minilang.LongVarDeclNode;
import com.wordpress.hextruffle.minilang.LongVarReadNode;
import com.wordpress.hextruffle.minilang.RootWrapperNode;

public class PrimalityTestBenchmark {
	public static void main(String[] args) {
		TruffleRuntime runtime = Truffle.getRuntime();
		RootNode checkSinglePrime = new RootWrapperNode(null); // todo
		CallTarget checkPrimeCallTarget = runtime.createCallTarget(checkSinglePrime);
		DirectCallNode callPrimeChecker = runtime.createDirectCallNode(checkPrimeCallTarget);	
		RootNode overallRoot = new RootWrapperNode(
				new ForLoopNode(
						new LongVarDeclNode("i", new LongLiteralNode(1)), new LessThanLongComparison(new LongVarReadNode("i"), new LongLiteralNode(10000000L)), new LongIncrementNode("i"),
						new CompoundOperationNode(new PrintDebugger(new LongVarReadNode("i")))
						)
				);
		CallTarget tgt = runtime.createCallTarget(overallRoot);
		for (int i = 0; i < 600; i++) {
			long start = System.currentTimeMillis();
			tgt.call();
			System.out.println(System.currentTimeMillis()-start+":"+i);
		}
	}
}
