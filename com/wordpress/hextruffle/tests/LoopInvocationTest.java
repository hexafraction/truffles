package com.wordpress.hextruffle.tests;

import java.util.Random;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.TruffleRuntime;
import com.oracle.truffle.api.CompilerDirectives.CompilationFinal;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.RootNode;

//-G:+TraceTruffleCompilationDetails -XX:+TraceDeoptimization -G:+TraceTrufflePerformanceWarnings -G:TruffleCompilationThreshold=5

public class LoopInvocationTest {
	public static class DivisibilityTestNode extends Node {
		final int target;
		
		public DivisibilityTestNode(int target) {
			super();
			this.target = target;
		}

		public boolean exec(long i, long m){
			return (i%m==0);
		}
	}
	
	
	public static class HardMathInnerNode extends Node {
		private @Child DivisibilityTestNode dtn = new DivisibilityTestNode(0);
		
		public long exec(long i){
			for(long m = 2; m < Math.sqrt(i); m++){
				// very inefficient primality test for testing performance
				if(dtn.exec(i, m)) return i;
				//if(i%m==0) return i;
			}
			return i+1;
		}
	}
	
	public static class HardMathNode extends Node {
		public HardMathNode(){
			this.innerNode = new HardMathInnerNode();
		}
		
		private @Child HardMathInnerNode innerNode;
		public long exec(long m) {
			long i;
			for (i = 0; i < 1_000_00; i++) {
				i  = innerNode.exec(i);
				
				
				if (CompilerDirectives.injectBranchProbability(0.00001, i % 1_000_000 == 0) && CompilerDirectives.injectBranchProbability(0.01, m == 42)) {
					//System.out.println("TRAP");

				}
			}
			return i;
		}
	}

	public static class ImmutableNode extends Node {

		private @Child
		HardMathNode child;

		public ImmutableNode() {
			this.child = new HardMathNode();
		}

		public long exec(Integer q) {
			long i = 0;
			for (long m = 0; m < 10; m++) {
				child.exec(m+q);
				//i += Math.min(0, child.exec());
			}
			i += child.exec(q);
			if (CompilerDirectives.inCompiledCode())
				return q + i;
			return -q - i;
		}

	}

	public static class TestNode extends RootNode {
		private @Child ImmutableNode child;

		public TestNode(ImmutableNode child) {
			super();
			this.child = child;
		}

		@Override
		public Object execute(VirtualFrame frame) {
			System.out.println(CompilerDirectives.inCompiledCode());
			return child.exec((Integer) frame.getArguments()[0]);
		}

	}

	public static void main(String[] args) {
		TruffleRuntime runtime = Truffle.getRuntime();
		TestNode root = new TestNode(new ImmutableNode());
		CallTarget tgt = runtime.createCallTarget(root);
		for (int i = 0; i < 60; i++) {
			long start = System.currentTimeMillis();
			long r = (long) tgt.call(i);
			System.out.println(System.currentTimeMillis()-start+":"+r);
		}
	}
}
