package com.wordpress.hextruffle.tests;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.TruffleRuntime;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.RootNode;

public class ChildNodeDemo {

	static class AdditionRootNode extends RootNode {
		@Child
		private TestChildNode left;
		@Child
		private TestChildNode right;

		@Override
		public Object execute(VirtualFrame frame) {
			return left.execute() + right.execute();
		}

		public AdditionRootNode(TestChildNode left, TestChildNode right) {
			super(null);
			this.left = left;
			this.right = right;
		}

	}

	static class TestChildNode extends Node {

		public TestChildNode() {
			super(null);
		}

		public int execute() {
			return 101;
		}
	}

	public static void main(String[] args) {
		TruffleRuntime runtime = Truffle.getRuntime();
	        TestChildNode leftChild = new TestChildNode();
	        TestChildNode rightChild = new TestChildNode();
	        AdditionRootNode root = new AdditionRootNode(leftChild, rightChild);
	        CallTarget target = runtime.createCallTarget(root);
	        System.out.println("Called the root node, which told us that 101 + 101 is "+target.call());
	        
	}
}
